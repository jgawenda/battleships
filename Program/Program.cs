﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameCore;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            /* The Rules
             * 
             * Total number of ships per user:
             * x1 - number of submarines
             * x2 - number of destroyers
             * x3 - number of cruisers
             * x4 - number of batteships
             */
            int x1 = 4;
            int x2 = 3;
            int x3 = 2;
            int x4 = 1;
            
            Game game = new Game(x1, x2, x3, x4);
            game.Start();

            
            Console.ReadLine();
        }
    }
}
