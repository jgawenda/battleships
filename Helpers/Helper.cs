﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public class Helper
    {
        public static char GetCharFromUser()
        {
            char charToReturn = ' ';
            string tempString = "";

            do
            {
                tempString = Console.ReadLine();
            } while (!Char.TryParse(tempString, out charToReturn));

            return charToReturn;
        }

        public static int GetIntFromUser()
        {
            int intToReturn = 0;
            string tempString = "";

            do
            {
                tempString = Console.ReadLine();
            } while (!Int32.TryParse(tempString, out intToReturn));

            return intToReturn;
        }
    }
}
