﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ships
{
    public class ShipX1 : Ship
    {
        public ShipX1(Orientation orientation, int x, int y) : base (orientation, x, y)
        {
            Size = 1;
            HitPoints = new char[] { '#' };

            SetCoordinates();
        }
    }
}
