﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ships
{
    public class ShipX2 : Ship
    {
        public ShipX2(Orientation orientation, int x, int y) : base (orientation, x, y)
        {
            Size = 2;
            HitPoints = new char[] { '#', '#' };

            SetCoordinates();
        }
    }
}
