﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ships
{
    public abstract class Ship
    {
        protected int Size { get; set; }
        public char[] HitPoints { get; protected set; }

        public Orientation Orientation { get; set; }

        public int X { get; protected set; }

        public int Y { get; protected set; }

        public int[,] Coordinates { get; protected set; }

        public Ship(Orientation orientation, int x, int y)
        {
            Orientation = orientation;
            X = x;
            Y = y;
        }
        
        protected void SetCoordinates()
        {
            Coordinates = new int[Size, 2];

            Coordinates[0, 0] = X;
            Coordinates[0, 1] = Y;

            if (Orientation == Orientation.Horizontal)
            {
                for (int i = 1; i < Size; i++)
                {
                    Coordinates[i, 0] = Coordinates[i - 1, 0] + 1;
                    Coordinates[i, 1] = Coordinates[i - 1, 1];
                }
            }
            else
            {
                for (int i = 1; i < Size; i++)
                {
                    Coordinates[i, 0] = Coordinates[i - 1, 0];
                    Coordinates[i, 1] = Coordinates[i - 1, 1] + 1;
                }
            }
        }

    }

    public enum Orientation
    {
        Horizontal,
        Vertical
    }
}
