﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ships
{
    public class ShipX4 : Ship
    {
        public ShipX4(Orientation orientation, int x, int y) : base (orientation, x, y)
        {
            Size = 4;
            HitPoints = new char[] { '#', '#', '#', '#' };

            SetCoordinates();
        }
    }
}
