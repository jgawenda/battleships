﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ships
{
    public class ShipX3 : Ship
    {
        public ShipX3(Orientation orientation, int x, int y) : base (orientation, x, y)
        {
            Size = 3;
            HitPoints = new char[] { '#', '#', '#' };

            SetCoordinates();
        }
    }
}
