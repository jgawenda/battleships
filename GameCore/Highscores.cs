﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GameCore
{
    class Highscores
    {
        public static void Add(int howManyShots)
        {
            Console.Clear();
            string filePath = @"C:\highscores.txt";
            Console.WriteLine("[Adding to the list of winners]");
            Console.Write("\nEnter your name (max 15 chars): ");

            string name = Console.ReadLine();

            if (name.Length > 15)
                name = name.Substring(0, 15);

            string stringToFile = String.Format("{0}\tName: {1}\tShots: {2}", DateTime.Now, name, howManyShots);
            
            try
            {
                File.AppendAllText(filePath, stringToFile);
                Console.WriteLine("\n\nScore saved in file: {0}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't save score. Error occured: {0}", ex.Message);
            }
        }
    }
}
