﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ships;
using Boards;
using Helpers;
using AI;

namespace GameCore
{
    public class Game
    {
        private Board _playerBoard = new Board();
        private Board _computerBoard = new Board();

        private int[] _ruleHowManyShips;
        private ArtificialIntelligence _ai = new ArtificialIntelligence();

        public Game(int x1, int x2, int x3, int x4)
        {
            _ruleHowManyShips = new int[] { x1, x2, x3, x4 };
        }

        private void ShowHelloMessage()
        {
            Console.WriteLine("Welcome to Battleships!");
            Console.WriteLine("Press Enter to start a game...");
        }

        private bool RollTheDice()
        {
            Random myRandom = new Random();
            int userRoll = 0;
            int computerRoll = 0;
            

            Console.WriteLine("Let's see who's going to be first...");
            Console.Write("Computer: ");

            int counter = 0;
            do
            {
                computerRoll = myRandom.Next(1, 10);
                Console.SetCursorPosition(10, 1);
                Console.Write(computerRoll);
                counter++;
            } while (counter < 5000);


            Console.ReadLine();
            Console.WriteLine("Player: ");

            counter = 0;
            do
            {
                userRoll = myRandom.Next(1, 10);
                Console.SetCursorPosition(8, 2);
                Console.Write(userRoll);
                counter++;
            } while (counter < 5000 || userRoll == computerRoll);


            Console.WriteLine("\n");

            if (userRoll > computerRoll)
            {
                Console.WriteLine("First turn makes player!");
                return true;
            }
            else
            {
                Console.WriteLine("First turn makes computer!");
                return false;
            }
        }

        private void DisplayBothBoards()
        {
            Console.SetCursorPosition(0, 2);
            Console.WriteLine("Enemy board:");
            _computerBoard.Display(false, 0, 3);

            Console.SetCursorPosition(50, 2);
            Console.WriteLine("Your board:");
            _playerBoard.Display(true, 50, 3);
        }

        private bool PlayerTurn()
        {
            char x = ' ';
            int y = 0;
            bool didItHit = false;

            Console.Clear();
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.CursorLeft = 30;
            Console.WriteLine("Captain, it's out turn. SHOOT AT THEM!");
            Console.BackgroundColor = ConsoleColor.Black;

            DisplayBothBoards();

            Console.WriteLine("----------------\n\nCaptain! Give us an order.");
            Console.WriteLine("Enter coordinates:");
            Console.Write("a-j: ");
            x = Helper.GetCharFromUser();
            Console.Write("0-9: ");
            y = Helper.GetIntFromUser();

            didItHit = _computerBoard.Shoot(x, y);

            Console.Clear();
            Console.WriteLine("");
            DisplayBothBoards();
            Console.WriteLine("----------------\n\n");

            if (didItHit)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("We HIT them!");
                Console.WriteLine("Press Enter to load next cannon.");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.ReadLine();
            }

            return didItHit;
        }

        private bool AITurn()
        {
            bool didItHit = false;

            Console.Clear();
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.CursorLeft = 30;
            Console.WriteLine("They are shooting at us! Watch out!");
            Console.BackgroundColor = ConsoleColor.Black;

            DisplayBothBoards();

            Console.WriteLine("----------------\n\nWe can't do much right now...");
            Console.WriteLine("Press Enter to prepare for shot");
            Console.ReadLine();

            didItHit = _ai.AIShoot(_playerBoard);

            Console.Clear();
            DisplayBothBoards();
            Console.WriteLine("----------------\n\n");

            if (didItHit)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("WE GOT HIT!");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.WriteLine("They missed. Lucky us...");
            }

            Console.WriteLine("Press Enter to proceed...");
            Console.ReadLine();

            return didItHit;
        }
        
        private void PrepareTheBoard()
        {
            Console.WriteLine("Prepare your board. Enter the coordinates to place your ships.");
            Console.WriteLine("Press Enter to begin...");
            
            //ADDING X1 SHIPS
            for (int i=0; i<_ruleHowManyShips[0]; i++)
            {
                char x = ' ';
                int y = 0;

                Orientation orientation = Orientation.Horizontal;

                do
                {
                    Console.Clear();
                    Console.WriteLine("Your board:\n");

                    _playerBoard.Display(true, 0, 0);

                    Console.WriteLine("----------------------\n");
                    Console.WriteLine("Place the submarine (#) number {0}", i + 1);
                    Console.WriteLine("Enter coordinates:");
                    Console.Write("a-j: ");
                    x = Helper.GetCharFromUser();
                    Console.Write("0-9: ");
                    y = Helper.GetIntFromUser();

                } while (!_playerBoard.AddShip(1, orientation, x, y));
            }

            //ADDING X2 SHIPS
            for (int i = 0; i < _ruleHowManyShips[1]; i++)
            {
                char x = ' ';
                int y = 0;

                Orientation orientation = Orientation.Horizontal;

                do
                {
                    Console.Clear();
                    Console.WriteLine("Your board:\n");

                    _playerBoard.Display(true, 0, 0);

                    Console.WriteLine("----------------------\n");
                    Console.WriteLine("Place the destroyer (##) number {0}", i + 1);
                    Console.WriteLine("Enter coordinates:");
                    Console.Write("a-j: ");
                    x = Helper.GetCharFromUser();
                    Console.Write("0-9: ");
                    y = Helper.GetIntFromUser();

                    Console.Write("0 - horizontal, 1 - vertical: ");
                    int tempInt = 0;
                    do
                    {
                        tempInt = Helper.GetIntFromUser();
                    } while (tempInt != 0 && tempInt != 1);

                    if (tempInt == 1)
                        orientation = Orientation.Vertical;

                } while (!_playerBoard.AddShip(2, orientation, x, y));
            }

            //ADDING X3 SHIPS
            for (int i = 0; i < _ruleHowManyShips[2]; i++)
            {
                char x = ' ';
                int y = 0;

                Orientation orientation = Orientation.Horizontal;

                do
                {
                    Console.Clear();
                    Console.WriteLine("Your board:\n");

                    _playerBoard.Display(true, 0, 0);

                    Console.WriteLine("----------------------\n");
                    Console.WriteLine("Place the cruiser (###) number {0}", i + 1);
                    Console.WriteLine("Enter coordinates:");
                    Console.Write("a-j: ");
                    x = Helper.GetCharFromUser();
                    Console.Write("0-9: ");
                    y = Helper.GetIntFromUser();

                    Console.Write("0 - horizontal, 1 - vertical: ");
                    int tempInt = 0;
                    do
                    {
                        tempInt = Helper.GetIntFromUser();
                    } while (tempInt != 0 && tempInt != 1);

                    if (tempInt == 1)
                        orientation = Orientation.Vertical;

                } while (!_playerBoard.AddShip(3, orientation, x, y));
            }

            //ADDING X4 SHIPS
            for (int i = 0; i < _ruleHowManyShips[3]; i++)
            {
                char x = ' ';
                int y = 0;

                Orientation orientation = Orientation.Horizontal;

                do
                {
                    Console.Clear();
                    Console.WriteLine("Your board:\n");

                    _playerBoard.Display(true, 0, 0);

                    Console.WriteLine("----------------------\n");
                    Console.WriteLine("Place the battleship (####) number {0}", i + 1);
                    Console.WriteLine("Enter coordinates:");
                    Console.Write("a-j: ");
                    x = Helper.GetCharFromUser();
                    Console.Write("0-9: ");
                    y = Helper.GetIntFromUser();

                    Console.Write("0 - horizontal, 1 - vertical: ");
                    int tempInt = 0;
                    do
                    {
                        tempInt = Helper.GetIntFromUser();
                    } while (tempInt != 0 && tempInt != 1);

                    if (tempInt == 1)
                        orientation = Orientation.Vertical;

                } while (!_playerBoard.AddShip(4, orientation, x, y));
            }

            
            Console.Clear();
            _playerBoard.Display(true, 0, 0);

            Console.WriteLine("----------------------\n");
            Console.WriteLine("Press Enter to continue...");
            Console.ReadLine();
        }

        private void PrepareTheBoardAI()
        {
            _ai.AIPrepareBoard(_computerBoard, _ruleHowManyShips);
        }

        private bool CheckIfGameOver(Board board)
        {
            return board.IsGameOver();
        }

        private void DisplayWin()
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.Clear();
            Console.WriteLine("WE'VE WON!");
            DisplayBothBoards();
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        private void DisplayLoose()
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.Clear();
            Console.WriteLine("WE LOST!");
            DisplayBothBoards();
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }

        public void Start()
        {
            ShowHelloMessage();
            Console.ReadLine();

            Console.WindowWidth = 100;
            Console.WindowHeight = 40;

            PrepareTheBoard();
            Console.WriteLine("Computer is preparing his board right now... Please wait a second.");
            PrepareTheBoardAI();
            Console.Clear();
            Console.WriteLine("Let's roll the dice to see who is going to start first. Press Enter to continue...");
            Console.ReadLine();
            Console.Clear();
            
            bool isPlayerFirst = RollTheDice();

            Console.WriteLine("\nPress Enter to continue...");
            Console.ReadLine();

            int shotsCounter = 0;
            bool isGameOverYet = false;

            if (isPlayerFirst)
            {
                do
                {
                    do
                    {
                        if (CheckIfGameOver(_computerBoard))
                        {
                            isGameOverYet = true;
                            DisplayWin();
                            Highscores.Add(shotsCounter);
                            break;
                        }
                        shotsCounter++;
                    } while (PlayerTurn());

                    if (isGameOverYet)
                        break;

                    do
                    {
                        if (CheckIfGameOver(_playerBoard))
                        {
                            isGameOverYet = true;
                            DisplayLoose();
                            break;
                        }
                    } while (AITurn());
                    
                } while (!isGameOverYet);
            }
            else
            {
                do
                {
                    do
                    {
                        if (CheckIfGameOver(_playerBoard))
                        {
                            isGameOverYet = true;
                            DisplayLoose();
                            break;
                        }
                    } while (AITurn());

                    if (isGameOverYet)
                        break;

                    do
                    {
                        if (CheckIfGameOver(_computerBoard))
                        {
                            isGameOverYet = true;
                            DisplayWin();
                            Highscores.Add(shotsCounter);
                            break;
                        }
                        shotsCounter++;
                    } while (PlayerTurn());
                    
                } while (!isGameOverYet);
            }

            Console.Clear();
            Console.WriteLine("\n\nThank you for playing");
        }
    }
}
