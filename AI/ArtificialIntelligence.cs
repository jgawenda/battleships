﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Boards;
using Ships;

namespace AI
{
    public class ArtificialIntelligence
    {
        private Random _random;

        private int[] _lastHitOnTarget = new int[] { -1, -1 };

        public ArtificialIntelligence()
        {
            _random = new Random();
        }

        public void AIPrepareBoard(Board board, int[] rules)
        {
            int tempOrientation = 0;
            Orientation orientation = Orientation.Horizontal;
            int x = 0;
            int y = 0;
            
            //ADDING #
            for (int i = 0; i < rules[0]; i++)
            {
                do
                {
                    x = _random.Next(0, 10);
                    y = _random.Next(0, 10);

                } while (!board.AddShip(1, orientation, x, y, false));
            }

            //ADDING ##
            for (int i = 0; i < rules[1]; i++)
            {
                do
                {
                    tempOrientation = _random.Next(0, 2);
                    if (tempOrientation == 0)
                        orientation = Orientation.Horizontal;
                    else
                        orientation = Orientation.Vertical;

                    x = _random.Next(0, 10);
                    y = _random.Next(0, 10);

                } while (!board.AddShip(2, orientation, x, y, false));
            }

            //ADDING ###
            for (int i = 0; i < rules[2]; i++)
            {
                do
                {
                    tempOrientation = _random.Next(0, 2);
                    if (tempOrientation == 0)
                        orientation = Orientation.Horizontal;
                    else
                        orientation = Orientation.Vertical;

                    x = _random.Next(0, 10);
                    y = _random.Next(0, 10);

                } while (!board.AddShip(3, orientation, x, y, false));
            }

            //ADDING ####
            for (int i = 0; i < rules[3]; i++)
            {
                do
                {
                    tempOrientation = _random.Next(0, 2);
                    if (tempOrientation == 0)
                        orientation = Orientation.Horizontal;
                    else
                        orientation = Orientation.Vertical;

                    x = _random.Next(0, 10);
                    y = _random.Next(0, 10);

                } while (!board.AddShip(4, orientation, x, y, false));
            }
        }

        public bool AIShoot(Board board)
        {
            int x = 0;
            int y = 0;
            bool didItHit = false;
            

            if ((_lastHitOnTarget[0] >= 0) && (_lastHitOnTarget[1] >= 0) && (board.ShowHitOrMissedShot(_lastHitOnTarget[0], _lastHitOnTarget[1]) == 'x'))
            {
                //check surroundings of last hit on target
                if ((_lastHitOnTarget[0] - 1) >= 0 && board.ShowHitOrMissedShot(_lastHitOnTarget[0] - 1, _lastHitOnTarget[1]) == ' ')
                {
                    didItHit = board.Shoot(_lastHitOnTarget[0] - 1, _lastHitOnTarget[1]);
                    if (didItHit)
                    {
                        _lastHitOnTarget[0] = _lastHitOnTarget[0] - 1;
                    }
                    return didItHit;
                }
                else if ((_lastHitOnTarget[0] + 1) < 10 && board.ShowHitOrMissedShot(_lastHitOnTarget[0] + 1, _lastHitOnTarget[1]) == ' ')
                {
                    didItHit = board.Shoot(_lastHitOnTarget[0] + 1, _lastHitOnTarget[1]);
                    if (didItHit)
                    {
                        _lastHitOnTarget[0] = _lastHitOnTarget[0] + 1;
                    }
                    return didItHit;
                }
                else if ((_lastHitOnTarget[1] - 1) >= 0 && board.ShowHitOrMissedShot(_lastHitOnTarget[0], _lastHitOnTarget[1] - 1) == ' ')
                {
                    didItHit = board.Shoot(_lastHitOnTarget[0], _lastHitOnTarget[1] - 1);
                    if (didItHit)
                    {
                        _lastHitOnTarget[1] = _lastHitOnTarget[1] - 1;
                    }
                    return didItHit;
                }
                else if ((_lastHitOnTarget[1] + 1) < 10 && board.ShowHitOrMissedShot(_lastHitOnTarget[0], _lastHitOnTarget[1] + 1) == ' ')
                {
                    didItHit = board.Shoot(_lastHitOnTarget[0], _lastHitOnTarget[1] + 1);
                    if (didItHit)
                    {
                        _lastHitOnTarget[1] = _lastHitOnTarget[1] + 1;
                    }
                    return didItHit;
                }
                //if nothing cool around, let's reset last hit on target and begin again with random shooting
                else
                {
                    _lastHitOnTarget[0] = -1;
                    _lastHitOnTarget[0] = -1;

                    //totally random
                    do
                    {
                        x = _random.Next(0, 10);
                        y = _random.Next(0, 10);
                    } while (!(board.ShowHitOrMissedShot(x, y) == ' '));

                    didItHit = board.Shoot(x, y);
                    if (didItHit)
                    {
                        _lastHitOnTarget[0] = x;
                        _lastHitOnTarget[1] = y;
                    }

                    return didItHit;
                }
            }
            // we dont rely on last hit on target down there
            else
            {
                // check whole board for damaged ships, if found - get it
                int tempX = 0;
                int tempY = 0;

                for (int i=0; i<10; i++)
                {
                    for (int j=0; j<10; j++)
                    {
                        if (board.ShowHitOrMissedShot(j, i) == 'x')
                        {
                            tempX = j;
                            tempY = i;
                            //Console.WriteLine("DAMAGED FOUND");
                            //check surroundings of damaged ship
                            if ((tempX - 1) >= 0 && board.ShowHitOrMissedShot(tempX - 1, tempY) == ' ')
                            {
                                didItHit = board.Shoot(tempX - 1, tempY);
                                if (didItHit)
                                {
                                    _lastHitOnTarget[0] = tempX - 1;
                                    _lastHitOnTarget[1] = tempY;
                                }
                                return didItHit;
                            }
                            if ((tempX + 1) < 10 && board.ShowHitOrMissedShot(tempX + 1, tempY) == ' ')
                            {
                                didItHit = board.Shoot(tempX + 1, tempY);
                                if (didItHit)
                                {
                                    _lastHitOnTarget[0] = tempX + 1;
                                    _lastHitOnTarget[1] = tempY;
                                }
                                return didItHit;
                            }
                            if ((tempY - 1) >= 0 && board.ShowHitOrMissedShot(tempX, tempY - 1) == ' ')
                            {
                                didItHit = board.Shoot(tempX, tempY - 1);
                                if (didItHit)
                                {
                                    _lastHitOnTarget[0] = tempX;
                                    _lastHitOnTarget[1] = tempY - 1;
                                }
                                return didItHit;
                            }
                            if ((tempY + 1) < 10 && board.ShowHitOrMissedShot(tempX, tempY + 1) == ' ')
                            {
                                didItHit = board.Shoot(tempX, tempY + 1);
                                if (didItHit)
                                {
                                    _lastHitOnTarget[0] = tempX;
                                    _lastHitOnTarget[1] = tempY + 1;
                                }
                                return didItHit;
                            }
                            //Console.WriteLine("Couldn't do anything with this one");
                        }
                    }
                }
                
                //totally random
                do
                {
                    x = _random.Next(0, 10);
                    y = _random.Next(0, 10);
                } while (!(board.ShowHitOrMissedShot(x, y) == ' '));

                didItHit = board.Shoot(x, y);
                if (didItHit)
                {
                    _lastHitOnTarget[0] = x;
                    _lastHitOnTarget[1] = y;
                }

                return didItHit;
            }

        }
    }
}
