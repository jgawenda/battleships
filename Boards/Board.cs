﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ships;

namespace Boards
{
    public class Board
    {
        private char[] _arrayOfChars = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j' };

        private List<int[,]> _missedShots;
        
        private List<Ship> _listOfShips;

        public Board()
        {
            _listOfShips = new List<Ship>();
            _missedShots = new List<int[,]>();
        }

        public void Display(bool showAllInfo, int cursorLeft, int cursorTop)
        {
            Console.CursorLeft = cursorLeft;
            Console.CursorTop = cursorTop;
            Console.Write("   ");

            for (int i=0; i<10; i++)
            {
                Console.Write(" {0} ", _arrayOfChars[i]);
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.Write("|");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            
            DisplaySeparator(cursorLeft);

            for (int i=0; i<10; i++)
            {
                Console.Write(" {0}", i);
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.Write("|");
                Console.ForegroundColor = ConsoleColor.Gray;

                for (int j=0; j<10; j++)
                {
                    Console.Write(" ");

                    /*
                     * old version
                    if (IsThereAShip(j, i))
                        Console.Write("#");
                    else
                        Console.Write(" ");
                    */

                    if (showAllInfo)
                        Console.Write(ShowAllHpOrMissedShot(j, i));
                    else
                        Console.Write(ShowHitOrMissedShot(j, i));
                    Console.ForegroundColor = ConsoleColor.Gray;

                    Console.Write(" ");

                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    Console.Write("|");
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                
                DisplaySeparator(cursorLeft);
            }

            //Console.WriteLine("Ships on board: {0}", _listOfShips.Count);
            Console.CursorLeft = cursorLeft;
            CountAndDisplayShips();
        }

        private void CountAndDisplayShips()
        {
            int x1 = 0;
            int x2 = 0;
            int x3 = 0;
            int x4 = 0;
            

            foreach (Ship ship in _listOfShips)
            {
                if (ship.HitPoints.Length == 1)
                {
                    foreach (char hp in ship.HitPoints)
                    {
                        if (hp == '#')
                        {
                            x1++;
                            break;
                        }
                    }
                }
                else if (ship.HitPoints.Length == 2)
                {
                    foreach (char hp in ship.HitPoints)
                    {
                        if (hp == '#')
                        {
                            x2++;
                            break;
                        }
                    }
                }
                else if (ship.HitPoints.Length == 3)
                {
                    foreach (char hp in ship.HitPoints)
                    {
                        if (hp == '#')
                        {
                            x3++;
                            break;
                        }
                    }
                }
                else if (ship.HitPoints.Length == 4)
                {
                    foreach (char hp in ship.HitPoints)
                    {
                        if (hp == '#')
                        {
                            x4++;
                            break;
                        }
                    }
                }
            }

            Console.WriteLine("#: {0}, ##: {1}, ###: {2}, ####: {3}", x1, x2, x3, x4);
        }

        private void DisplaySeparator(int cursorLeft)
        {
            Console.CursorLeft = cursorLeft;
            Console.WriteLine("");
            Console.CursorLeft = cursorLeft;
            Console.ForegroundColor = ConsoleColor.DarkGray;
            for (int i=0; i<43; i++)
            {
                if (i % 2 == 0)
                    Console.Write("-");
                else
                    Console.Write(" ");

            }

            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine("");
            Console.CursorLeft = cursorLeft;
        }

        private int TranslateCharToInt(char letter)
        {
            //find the index of a specified char
            int index = 0;
            try
            {
                index = Array.FindIndex(_arrayOfChars, p => p.Equals(letter));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occured: {0}", ex.Message);
            }

            return index;
        }

        public bool IsGameOver()
        {
            bool gameIsOver = true;

            foreach (Ship ship in _listOfShips)
            {
                foreach (char hp in ship.HitPoints)
                {
                    if (hp == '#')
                    {
                        gameIsOver = false;
                        break;
                    }
                }
                if (!gameIsOver)
                    break;
            }

            return gameIsOver;
        }

        private bool IsThereAShip(int x, int y)
        {
            bool flag = false;
            foreach (Ship element in _listOfShips)
            {
                for (int i = 0; i < element.Coordinates.GetLength(0); i++)
                {
                    if (element.Coordinates[i, 0] == x && element.Coordinates[i, 1] == y)
                    {
                        flag = true;
                        break;
                    }
                }

                if (flag)
                    break;
            }

            if (flag)
                return true;
            else
                return false;
        }
        
        public bool AddShip(int size, Orientation orientation, int x, int y, bool showWarnings)
        {
            bool error = false;
            int tempX = x;
            int tempY = y;
            if (!IsThereAShip(x, y))
            {
                if (orientation == Orientation.Horizontal)
                {
                    for (int i = 1; i < size; i++)
                    {
                        tempX++;
                        if (IsThereAShip(tempX, y) || tempX > 9 || y > 9)
                        {
                            Console.WriteLine("There is another ship blocking this place or new ship is out of board");
                            error = true;
                            break;
                        }
                    }
                }
                else
                {
                    for (int i = 1; i < size; i++)
                    {
                        tempY++;
                        if (IsThereAShip(x, tempY) || x > 9 || tempY > 9)
                        {
                            Console.WriteLine("There is another ship blocking this place or new ship is out of board");
                            error = true;
                            break;
                        }
                    }
                }

                if (error)
                {
                    Console.WriteLine("Ship hasn't been added");
                    if (showWarnings)
                        Console.ReadLine();
                    return false;
                }
                else
                {
                    switch (size)
                    {
                        case 1:
                            _listOfShips.Add(new ShipX1(orientation, x, y));
                            //Console.WriteLine("New ship (X1) added!");
                            break;
                        case 2:
                            _listOfShips.Add(new ShipX2(orientation, x, y));
                            //Console.WriteLine("New ship (X2) added!");
                            break;
                        case 3:
                            _listOfShips.Add(new ShipX3(orientation, x, y));
                            //Console.WriteLine("New ship (X3) added!");
                            break;
                        case 4:
                            _listOfShips.Add(new ShipX4(orientation, x, y));
                            //Console.WriteLine("New ship (X4) added!");
                            break;
                        default:
                            Console.WriteLine("You can't add a ship of that size");
                            if (showWarnings)
                                Console.ReadLine();
                            break;
                    }

                    if (size > 0 && size < 5)
                        return true;
                    else
                        return false;
                }
            }
            else
            {
                Console.WriteLine("Another ship is already in this location.");
                if (showWarnings)
                    Console.ReadLine();
                return false;
            }
        }
        
        public bool AddShip(int size, Orientation orientation, char x, int y)
        {
            int xTrans = TranslateCharToInt(x);

            //did we find the letter?
            if (xTrans < 0 || y > 9)
            {
                Console.WriteLine("You can't place your ship there! You have to enter a letter from \'a\' to \'j\' and number from 0 to 9");
                Console.ReadLine();
                return false;
            }

            return AddShip(size, orientation, xTrans, y, true);
        }

        private char ShowAllHpOrMissedShot(int x, int y)
        {
            //search for the ship's HP
            foreach (Ship element in _listOfShips)
            {
                for (int i = 0; i < element.Coordinates.GetLength(0); i++)
                {
                    if (element.Coordinates[i, 0] == x && element.Coordinates[i, 1] == y)
                    {
                        if (element.HitPoints[i] == 'X')
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                        else if (element.HitPoints[i] == 'x')
                            Console.ForegroundColor = ConsoleColor.Red;
                        return element.HitPoints[i];
                    }
                }
            }

            //search for the missed shots
            foreach (int[,] row in _missedShots)
            {
                if (row[0, 0] == x && row[0, 1] == y)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    return '*';
                }
            }

            //if none of above is present, simply show space char (blank field)
            Console.ForegroundColor = ConsoleColor.Gray;
            return ' ';
        }

        public char ShowHitOrMissedShot(int x, int y)
        {
            //search for the ship's hits (not showing its "alive" parts)
            foreach (Ship element in _listOfShips)
            {
                for (int i = 0; i < element.Coordinates.GetLength(0); i++)
                {
                    if (element.Coordinates[i, 0] == x && element.Coordinates[i, 1] == y)
                    {
                        if (element.HitPoints[i] == 'X')
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                            return element.HitPoints[i];
                        }
                        else if (element.HitPoints[i] == 'x')
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            return element.HitPoints[i];
                        }
                    }
                }
            }

            //search for the missed shots
            foreach (int[,] row in _missedShots)
            {
                if (row[0, 0] == x && row[0, 1] == y)
                {
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    return '*';
                }
            }

            //if none of above is present, simply show space char (blank field)
            Console.ForegroundColor = ConsoleColor.Gray;
            return ' ';
        }

        private bool HitTheShip(int x, int y)
        {
            foreach (Ship ship in _listOfShips)
            {
                for (int i=0; i<ship.Coordinates.GetLength(0); i++)
                {
                    if (ship.Coordinates[i, 0] == x && ship.Coordinates[i, 1] == y)
                    {
                        if (ship.HitPoints[i] == '#')
                        {
                            ship.HitPoints[i] = 'x';

                            int counter = 0;
                            foreach (char hp in ship.HitPoints)
                            {
                                if (hp == 'x')
                                    counter++;
                            }

                            if (counter == ship.HitPoints.Length)
                            {
                                for (int a = 0; a < ship.HitPoints.Length; a++)
                                    ship.HitPoints[a] = 'X';
                            }

                            return true;
                        }

                        return false;
                    }
                }
            }
            return false;
        }
        
        public bool Shoot(int x, int y)
        {
            if (IsThereAShip(x, y))
            {
                return HitTheShip(x, y);
            }
            else
            {
                //see if there is a missed shot already saved
                bool flag = false;
                foreach (int[,] row in _missedShots)
                {
                    if (row[0,0] == x && row[0,1] == y)
                    {
                        flag = true;
                        break;
                    }
                }

                if (!flag)
                {
                    _missedShots.Add(new int[,] { { x, y } });
                }

                return false;
            }
        }

        public bool Shoot(char x, int y)
        {
            int xTrans = TranslateCharToInt(x);

            //did we find the letter?
            if (xTrans < 0 || y > 9)
            {
                Console.WriteLine("You shot out of bounds! You have to enter a letter from \'a\' to \'j\' and number from 0 to 9");
                Console.ReadLine();
                return false;
            }

            return Shoot(xTrans, y);

        }
        
    }
}
